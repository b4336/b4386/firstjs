function showGalaxyAndAlt(element) {
    document.getElementById("galaxyDiv").innerHTML = element.alt;
    document.getElementById("galaxyDiv").style.backgroundImage = "url(" + element.src + ")";
    document.getElementById("galaxyDiv").style.backgroundColor = "#16EE34"; 
}

function unshowGalaxyAndAlt() {
    document.getElementById("galaxyDiv").style.backgroundImage = ""; 
    document.getElementById("galaxyDiv").style.backgroundColor = "#c0c0ce";
    document.getElementById("galaxyDiv").innerHTML = "This is the image where you mouse is now on; its alt text is shown as well.";
    // How can we put this string above into a variable? const?
}

var countries = [];
function addCountries() {
    var country = prompt("What country do you want to add?");
    countries[countries.length] = " " + country;
    document.getElementById("countriesList").innerHTML = countries;
}

function nicknameInput() {
    if (document.getElementById("withNickname").checked) {
        document.getElementById("nickname").style.display = "inline";
        document.getElementById("nicknameField").setAttribute("required", true);
    } else {
        document.getElementById("nicknameField").removeAttribute("required");
        document.getElementById("nickname").style.display = "none";
    }
}

function checkEmail() {
    var email1 = document.getElementById("email1");
    var email2 = document.getElementById("email2");
    if (email1.value != email2.value) {
        window.alert("The two emails you entered do not match!");
        return false;
    }
}